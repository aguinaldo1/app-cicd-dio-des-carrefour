#!/bin/bash

echo "Criando as imagens...."

docker build -t aguinaldoamerico/projeto-backend:1.1 backend/.
docker build -t aguinaldoamerico/projeto-database:1.1 database/.

echo "Realizando o push das imagens...."

docker push aguinaldoamerico/projeto-backend:1.1
docker push aguinaldoamerico/projeto-database:1.1

echo "Criando os deployments...."

kubectl apply -f ./deployment.yml